﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataMining_lab1_AprioriAlgorithm;
using System.Collections.Generic;
using System.Collections;

namespace Unit.Tests
{
    [TestClass]
    public class BaseAlgorithmTests
    {
        Dictionary<int, List<string>> transactions = new Dictionary<int, List<string>>
        {
            {100, new List<string>{"хлеб", "молоко", "печенье"}},
            {200, new List<string>{"молоко", "сметана"}},
            {300, new List<string>{"молоко", "хлеб", "сметана", "печенье"}},
            {400, new List<string>{"колбаса", "сметана"}},
            {500, new List<string>{"хлеб", "молоко", "печенье", "сметана"}},
            {600, new List<string>{"конфеты"}}
        };

        Dictionary<int, List<string>> transactions2 = new Dictionary<int, List<string>>
        {
            {100, new List<string>{"I1", "I2", "I5" } },
            {200, new List<string>{"I2", "I4" } },
            {300, new List<string>{"I2", "I3" } },
            {400, new List<string>{"I1", "I2", "I4" } },
            {500, new List<string>{"I1", "I3" } },
            {600, new List<string>{"I2", "I3" } },
            {700, new List<string>{"I1", "I3" } },
            {800, new List<string>{"I1", "I2" ,"I3", "I5" } },
            {900, new List<string>{"I1", "I2", "I3"  } }
        };

        [TestMethod]
        public void GetSupport()
        {
            var ba = new TransactionsTable(new List<IEnumerable<string>>(transactions.Values));
            var supData = new List<string> { "хлеб", "молоко", "печенье" };
            var sup = ba.GetSupportNumber(new ItemSet(supData));
            Assert.AreEqual(3, sup);
            supData = new List<string> { "конфеты" };
            sup = ba.GetSupportNumber(new ItemSet(supData));
            Assert.AreEqual(1, sup);
            supData = new List<string> { "молоко", "печенье" };
            sup = ba.GetSupportNumber(new ItemSet(supData));
            Assert.AreEqual(3, sup);
        }

        [TestMethod]
        public void TestApriori()
        {
            var tt = new TransactionsTable(new List<IEnumerable<string>>(transactions2.Values));
            var res = BaseAlgorithm.Apriori_GetFrequentItemset(tt, 0.22f);
            var rules = BaseAlgorithm.GenerateRules(tt, res, 0.7);
            int i = 1;
        }

        [TestMethod]
        public void TestInputDb()
        {
            var tt = new TransactionsTable(new List<IEnumerable<string>>(transactions2.Values));
            var str = tt.ToString();
            var i = 2;
        }

        [TestMethod]
        public void TestApriori2()
        {
            var sampleData = new List<List<string>>() {
                new List<string>(){"1", "3", "4" },
                new List<string>(){"2", "3", "5" },
                new List<string>(){"1", "2", "3", "5" },
                new List<string>(){"2", "5" }
            };
            var tt = new TransactionsTable(sampleData);
            var str = tt.ToString();
            var res = BaseAlgorithm.Apriori_GetFrequentItemset(tt, 0.5f);
            var i = 2;
        }
    }

}