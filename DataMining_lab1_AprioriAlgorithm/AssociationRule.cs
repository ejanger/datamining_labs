﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMining_lab1_AprioriAlgorithm
{
    public class AssociationRule
    {
        private ItemSet From, To;
        double confidence;

        public AssociationRule(ItemSet from, ItemSet to, double confidence)
        {
            this.From = from;
            this.To = to;
            this.confidence = confidence;
        }

        public override string ToString()
        {
            return From.ToString() + " --> " + To.ToString() + " | confidence: " + confidence;
        }
    }
}
