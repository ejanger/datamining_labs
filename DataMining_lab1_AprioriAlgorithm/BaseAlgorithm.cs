﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMining_lab1_AprioriAlgorithm
{
    public class BaseAlgorithm
    {

        public static List<ItemSet> Apriori_GetFrequentItemset(TransactionsTable transTable, float minimalSupport)
        {
            var currentFrequentItemset = transTable.GetOneItemSets(minimalSupport);
            var resultFrequentItemset = new List<ItemSet>(currentFrequentItemset);

            for (int k = 2; currentFrequentItemset.Count > 0; k++)
            {
                var candidateItemset = GenerateCandidateItemset(transTable,currentFrequentItemset, k);

                currentFrequentItemset = new List<ItemSet>(candidateItemset
                    .Where(item => transTable.GetSupportRelative(item) >= minimalSupport));
                foreach (var curFreqItem in currentFrequentItemset)
                {
                    resultFrequentItemset.Add(curFreqItem);
                }
            }

            return resultFrequentItemset;
        }

        /// <summary>
        /// Герерирует наборы длины k из наборов currentFrequentItemset
        /// длины k-1, которые будут кандидатами в поддерживаемые
        /// </summary>
        /// <param name="currentFrequentItemset"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        private static List<ItemSet> GenerateCandidateItemset(TransactionsTable transTable, List<ItemSet> currentFrequentItemset, int k)
        {
            var curItemset = currentFrequentItemset.Where(item => item.Transaction.Count == k - 1);
            
            List<ItemSet> results = new List<ItemSet>();
            foreach (var curItemK_1 in curItemset)
            {
                var candK_2 = curItemset.Where(itemset => EqualsFirstElements(curItemK_1, itemset, k - 2));
                var lastItems = new HashSet<string>(candK_2.Select(items => items.Transaction[items.Transaction.Count - 1]));
                var lastItem = curItemK_1.Transaction[curItemK_1.Transaction.Count - 1];
                foreach (var validLastItem in lastItems.Where(li => li.CompareTo(lastItem)>0))
                {
                    var res = new List<string>(curItemK_1.Transaction);
                    res.Add(validLastItem);
                    var itemSet = new ItemSet(res);
                    itemSet.SupportCount = transTable.GetSupportNumber(itemSet);
                    results.Add(itemSet);
                }
            }
            results.Sort(transTable);
            return results;
        }

        public static bool EqualsFirstElements(ItemSet x, ItemSet y, int n)
        {
            for (int i = 0; i < Math.Min(Math.Min(x.Transaction.Count, y.Transaction.Count), n);
            i++)
            {
                int curItemCmp = x.Transaction[i].CompareTo(y.Transaction[i]);
                if (curItemCmp != 0)
                {
                    return false;
                }
            }
            return true;
        }

        public static List<AssociationRule> GenerateRules(TransactionsTable transTable, List<ItemSet> itemsSets,
            double minimalConfidence)
        {
            var res = new List<AssociationRule>();
            foreach (var itemset in itemsSets)
            {
                var unionSupport = transTable.GetSupportNumber(itemset);
                foreach (var fromItems in produceEnumeration(itemset.Transaction))
                {
                    if (fromItems.Count <= 0 || fromItems.Count == itemset.Transaction.Count)
                    {
                        continue;
                    }
                    var fromItemset = new ItemSet(fromItems);
                    var fromSupport = transTable.GetSupportNumber(fromItemset);
                    double curConfidence = (double)unionSupport / (double)fromSupport;
                    if (curConfidence >= minimalConfidence)
                    {
                        res.Add(new AssociationRule(fromItemset,
                            ItemsetSubstract(itemset, fromItemset),
                            curConfidence));
                    }
                }
            }
            return res;
        }

        private static ItemSet ItemsetSubstract(ItemSet from, ItemSet val)
        {
            var transres = from.Transaction.Where
                (el => !val.Transaction.Contains(el)).ToList();
            return new ItemSet(transres);
        }

        private static IEnumerable<int> constructSetFromBits(int i)
        {
            for (int n = 0; i != 0; i /= 2, n++)
            {
                if ((i & 1) != 0)
                    yield return n;
            }
        }

        /// <summary>
        /// http://stackoverflow.com/questions/10515449/generate-all-combinations-for-a-list-of-strings
        /// </summary>
        /// <param name="allValues"></param>
        /// <returns></returns>
        private static IEnumerable<List<string>> produceEnumeration(List<string> allValues)
        {
            for (int i = 0; i < (1 << allValues.Count); i++)
            {
                yield return
                    constructSetFromBits(i).Select(n => allValues[n]).ToList();
            }
        }

        public static List<List<string>> produceList(List<string> allValues)
        {
            return produceEnumeration(allValues).ToList();
        }
    }
}
