﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMining_lab1_AprioriAlgorithm
{
    public class TransactionsTable : IComparer<ItemSet>
    {
        public List<ItemSet> Transactions { get; set; }

        public TransactionsTable(IEnumerable<IEnumerable<string>> transactions)
        {
            Transactions = new List<ItemSet>(transactions.Select(el => new ItemSet(el)))
                .OrderBy(el => el, this).ToList();
            SupportCalc();
        }

        public static TransactionsTable ParseJSON(string json)
        {
            var inputData = new List<List<string>>();
            dynamic jsonData = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            foreach (var trans in jsonData.Transactions)
            {
                var curTrans = new List<string>();
                foreach (var kvp
                    in (trans as JObject).ToObject<Dictionary<string, string>>())
                {
                    curTrans.Add(kvp.Key + ":" + kvp.Value);
                }
                inputData.Add(curTrans);
            }
            return new TransactionsTable(inputData);
        }

        private void SupportCalc()
        {
            foreach (var itemset in Transactions)
            {
                itemset.SupportCount = GetSupportNumber(itemset);
            }
        }

        public int GetSupportNumber(ItemSet itemset)
        {
            int res = 0;
            foreach (var transaction in Transactions)
            {
                if (transaction.Contains(itemset))
                {
                    res++;
                }
            }
            return res;
        }

        public double GetSupportRelative(ItemSet itemset)
        {
            return (double)GetSupportNumber(itemset) / (double)Transactions.Count;
        }

        public int Compare(ItemSet x, ItemSet y)
        {
            for (int i = 0; i < Math.Min(x.Transaction.Count, y.Transaction.Count);
            i++)
            {
                int curItemCmp = x.Transaction[i].CompareTo(y.Transaction[i]);
                if (curItemCmp != 0)
                {
                    return curItemCmp;
                }
            }
            if (x.Transaction.Count < y.Transaction.Count)
            {
                return -1;
            }
            else if (x.Transaction.Count > y.Transaction.Count)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public override string ToString()
        {
            var res = "";
            foreach (var trans in Transactions)
            {
                res += trans.ToString() + Environment.NewLine;
            }
            return res;
        }

        /// <summary>
        /// Возвращает одноэлементные наборы,
        /// уровень поддержки которых не меньше заданной
        /// </summary>
        /// <param name="minimalSupport"></param>
        /// <returns></returns>
        public List<ItemSet> GetOneItemSets(float minimalSupport)
        {
            var allItems = new HashSet<string>();
            foreach (var transaction in Transactions)
            {
                transaction.Transaction.ForEach(item => allItems.Add(item));
            }
            return 
                allItems.Select
                (item => {
                    var itemSet = new ItemSet(new string[] { item });
                    itemSet.SupportCount = this.GetSupportNumber(itemSet);
                    return itemSet;
                })
                .Where(item => GetSupportRelative(item) >= minimalSupport)
                .ToList();
        }

    }
}
