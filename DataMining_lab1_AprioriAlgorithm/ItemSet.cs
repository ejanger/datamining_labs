﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMining_lab1_AprioriAlgorithm
{
    /// <summary>
    /// Набор объектов (транзакция).
    /// Поддерживает лексикографисекий порядок элементов.
    /// </summary>
    public class ItemSet : IComparer<string>
    {
        public int? SupportCount { get; set; }

        public List<string> Transaction { get; protected set; }

        public ItemSet(IEnumerable<string> items)
        {
            SupportCount = null;
            Transaction = new List<string>(items).OrderBy(el => el, this).ToList();
        }

        public int Compare(string x, string y)
        {
            return x.CompareTo(y);
        }

        public override string ToString()
        {
            string res = "";
            foreach (var val in Transaction)
            {
                res += " <" + val + "> ";
            }
            if (SupportCount.HasValue)
            {
                res += " | SupportCount=" + SupportCount.ToString();
            }
            return res;
        }

        public bool Contains(ItemSet itemset)
        {
            return itemset.Transaction.All(item => Transaction.Contains(item));
        }
    }
}
