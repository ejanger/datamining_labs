﻿using DataMining_lab1_AprioriAlgorithm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Lab1_Apriori
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonData = File.ReadAllText(args[0]);
            var tt = TransactionsTable.ParseJSON(jsonData);
            Console.WriteLine(tt.ToString());
            var cult = System.Globalization.CultureInfo.InvariantCulture;
            var res = BaseAlgorithm.Apriori_GetFrequentItemset(tt, (float)double.Parse(args[1], cult)/*0.15f*/);
            var rules = BaseAlgorithm.GenerateRules(tt, res, double.Parse(args[2], cult)/*0.9*/);
            Console.WriteLine(ToString(res));
            Console.WriteLine(ToString(rules));
        }

        public static string ToString<T>(List<T> itemsets)
        {
            var res = "";
            foreach (var trans in itemsets)
            {
                res += trans.ToString() + Environment.NewLine;
            }
            return res;
        }
    }
}
