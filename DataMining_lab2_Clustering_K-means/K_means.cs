﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataMining_lab2_Clustering_K_means
{
    public static class K_means
    {
        public static void Clustering<T>(List<T> data, T[] firstClustersCenter,
            Func<T, T, double> calcDistance, 
            Func<IEnumerable<T>, T> getNewCenter,
            double persicion,
            out byte[] clusterIndex,
            out T[] clusterCenter
            )
        {
            var pointNumber = data.Count;
            var indexCluster = new byte[pointNumber];
            var clusterNumber = firstClustersCenter.Length;
            clusterCenter = new T[clusterNumber];
            UpdateClusterIndex(data, firstClustersCenter, calcDistance, indexCluster);
            var newClusterCenter = new T[clusterNumber];
            Array.Copy(firstClustersCenter, newClusterCenter, clusterNumber);
            do
            {
                Array.Copy(newClusterCenter, clusterCenter, clusterNumber);
                for (byte clInd = 0; clInd < clusterNumber; clInd++)
                {
                    newClusterCenter[clInd] = getNewCenter(
                        data.Where((el, ind) => indexCluster[ind] == clInd));
                }
                UpdateClusterIndex(data, newClusterCenter, calcDistance, indexCluster);
            }
			//логичней использовать функцию .Max() > persicion 
            while (newClusterCenter.Zip(clusterCenter, (a,b) => calcDistance(a,b)).Min() > persicion);
            Array.Copy(newClusterCenter, clusterCenter, clusterNumber);
            clusterIndex = indexCluster;
        }

        private static void UpdateClusterIndex<T>(List<T> data, T[] clustersCenter, Func<T, T, double> calcDistance, byte[] clusterIndex)
        {
            var pointNumber = data.Count;
            var clusterNumber = clustersCenter.Length;
            for (int i = 0; i < pointNumber; i++)
            {
                byte clustInd = 0;
                var min = calcDistance(clustersCenter[clustInd], data[i]);
                for (byte clInd = 1; clInd < clusterNumber; clInd++)
                {
                    var curMin = calcDistance(clustersCenter[clInd], data[i]);
                    if (curMin < min)
                    {
                        min = curMin;
                        clustInd = clInd;
                    }
                }
                clusterIndex[i] = clustInd;
            }
        }
    }
}
