﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataMining_lab2_Clustering_K_means;

namespace Lab2_Clustering
{
    public partial class Form1 : Form
    {
        private Bitmap original, clustered;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                //dlg.Filter = "bmp files (*.bmp)|*.bmp";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    original = new Bitmap(dlg.FileName);
                    clustered = null;
                    pictureBox1.Image = original;
                }
            }
        }

        private void ClusteredImage()
        {
            Bitmap bmp = new Bitmap(pictureBox1.Image);
            var pixs = new List<Color>();
            for (int i = 0; i<bmp.Width; i++)
            {
                for (int j=0; j<bmp.Height; j++)
                {
                    pixs.Add(bmp.GetPixel(i, j));
                }
            }
            int clusterNum = 3;
            var firstCenter = new List<Color>();
            for (int i = 0; i<clusterNum; i++)
            {
                //firstCenter.Add(pixs[i]);
                
            }
            firstCenter.Add(Color.Red);
            firstCenter.Add(Color.Green);
            firstCenter.Add(Color.Blue);

            byte[] clusterInd;
            Color[] clusterCenter;
            K_means.Clustering<Color>(pixs, firstCenter.ToArray(),
                (c1, c2) => Math.Sqrt(Math.Pow(c1.R - c2.R, 2) +
                    Math.Pow(c1.G - c2.G, 2) +
                    Math.Pow(c1.B - c2.B, 2)),
                    GetNewCenter,
                    10,
                    out clusterInd,
                    out clusterCenter);
            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    bmp.SetPixel(i, j, clusterCenter[clusterInd[i*bmp.Height + j]]);
                }
            }
            for (int i = 0; i < clusterCenter.Length; i++)
            {
                Log(string.Format("Кластер {0}. Цвет: {1}", i, clusterCenter[i].ToString()));
            }
            clustered = bmp;
            pictureBox1.Image = bmp;
        }

        private Color GetNewCenter(IEnumerable<Color> data)
        {
            long r, g, b;
            r = g = b = 0;
            foreach (var c in data)
            {
                r += c.R;
                g += c.G;
                b += c.B;
            }
            var len = data.Count();
            if (len == 0)
            {
               return Color.FromArgb(255 / 3, 255 / 3, 255 / 3);
            }
            r /= len;
            g /= len;
            b /= len;
            return Color.FromArgb((int)r, (int)g, (int)b);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                if (original != null)
                {
                    if (clustered == null)
                    {
                        ClusteredImage();
                    }
                    pictureBox1.Image = clustered;
                }

            }
            else
            {
                if (original != null)
                {
                    pictureBox1.Image = original;
                }
            }
        }

        private void Log(string msg)
        {
            richTextBox1.AppendText(msg + Environment.NewLine);
            richTextBox1.Select(richTextBox1.Text.Length, 0);
        }
    }
}
